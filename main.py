import os, requests, threading, datetime, random
from pyvirtualdisplay import Display
from selenium import webdriver
from time import sleep

def send_start(worker_name):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/beacon/', data={'worker': worker_name}, timeout=5)
    except:
        print('err sent_beacon()')

def send_finish(worker_name, th, ah):
    try:
        requests.post('http://anthill.duckdns.org:1784/cms/', data={'worker': worker_name, 'th':th, 'ah':ah}, timeout=5)
    except:
        print('err send_finish()')

def main():
    with open('id', 'r') as f:
        worker_name = f.read()
    with open('node', 'r') as f:
        node = f.read()
    send_start(worker_name)
    display = Display(visible=0, size=(1366, 768))
    display.start()
    driver = webdriver.Firefox()
    driver.get(node)
    sleep(60*random.randint(5,20))
    th = driver.find_element_by_id('th').text
    ah = driver.find_element_by_id('ah').text
    driver.close()
    display.stop()
    send_finish(worker_name, th, ah)

if __name__ == '__main__':
    main()
#2018-02-26 15:10:01.426029
#2018-02-26 15:29:01.922359
#2018-02-26 15:53:01.254928
#2018-02-26 16:20:01.813438
#2018-02-26 17:20:01.473326
#2018-02-26 18:18:01.971177
#2018-02-26 19:03:01.593225
#2018-02-26 20:18:01.430382
#2018-02-26 21:17:01.396425
#2018-02-26 22:17:01.320685
#2018-02-26 23:37:02.171074
#2018-02-27 01:16:01.587126
#2018-02-27 02:01:02.084412